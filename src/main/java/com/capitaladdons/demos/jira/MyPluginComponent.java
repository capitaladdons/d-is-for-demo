package com.capitaladdons.demos.jira;

public interface MyPluginComponent
{
    String getName();
}