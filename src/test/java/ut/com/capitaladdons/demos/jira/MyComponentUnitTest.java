package ut.com.capitaladdons.demos.jira;

import org.junit.Test;
import com.capitaladdons.demos.jira.MyPluginComponent;
import com.capitaladdons.demos.jira.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}